package ru.fintech.homework.main

import ru.fintech.homework.base.MvpPresenter

interface MainMvpPresenter<V : MainMvpView> : MvpPresenter<V> {

    fun addNewDocument()

    fun deleteCurrentDocument(count: Int)

}
