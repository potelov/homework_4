package ru.fintech.homework.main

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.airbnb.lottie.LottieAnimationView
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.floatingactionbutton.FloatingActionButton
import ru.fintech.homework.R
import ru.fintech.homework.base.BaseFragment
import ru.fintech.homework.document.DocumentFragment

class MainFragment : BaseFragment(), MainMvpView {

    companion object {
        fun newInstance(): MainFragment {
            val args = Bundle()
            val fragment = MainFragment()
            fragment.arguments = args
            return fragment
        }

        const val ARGUMENT_DESCRIPTION: String = "description"
    }

    private lateinit var presenter: MainPresenter<MainMvpView>
    private lateinit var bottomAppBar: BottomAppBar
    private lateinit var fab: FloatingActionButton
    private lateinit var description: TextView
    private lateinit var lottieAnimation: LottieAnimationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        presenter = MainPresenter()
        presenter.onAttach(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_main, container, false)
        with(root) {
            bottomAppBar = (findViewById<BottomAppBar>(R.id.main_bottom_app_bar)).apply {
                inflateMenu(R.menu.bottom_appbar_menu)
                setOnMenuItemClickListener { item ->
                    if (item.itemId == R.id.menu_item_delete) {
                        presenter.deleteCurrentDocument(childFragmentManager.backStackEntryCount)
                    }
                    false
                }
            }
            fab = (findViewById<FloatingActionButton>(R.id.fab)).also {
                it.setOnClickListener { presenter.addNewDocument() }
            }
            description = findViewById(R.id.tv_description)
            lottieAnimation = findViewById(R.id.view_animation)
        }
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            val text = savedInstanceState.getString(ARGUMENT_DESCRIPTION)
            if (!TextUtils.isEmpty(text)) {
                description.text = text
            } else {
                hideDescription()
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        if (description.visibility == View.VISIBLE) {
            outState.putString(ARGUMENT_DESCRIPTION, description.text.toString())
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
    }

    override fun showDocumentFragment() {
        replaceFragment(DocumentFragment.newInstance())
    }

    override fun closeDocumentFragment() {
        popBackStack()
    }

    override fun hideDescription() {
        if (description.visibility == View.VISIBLE) {
            description.visibility = View.GONE
        }
    }

    override fun showAddAnimation() {
        lottieAnimation.setAnimation(R.raw.added)
        lottieAnimation.playAnimation()
        lottieAnimation.repeatCount = 0
    }

    override fun showDeleteAnimation() {
        lottieAnimation.setAnimation(R.raw.delete_bubble)
        lottieAnimation.playAnimation()
        lottieAnimation.repeatCount = 0
    }

    override fun showEmptyAnimation() {
        lottieAnimation.setAnimation(R.raw.dino_dance)
        lottieAnimation.playAnimation()
        lottieAnimation.repeatCount = 999999
        description.visibility = View.VISIBLE
        description.text = getString(R.string.main_empty_document)
    }
}
