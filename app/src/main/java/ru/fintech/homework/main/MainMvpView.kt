package ru.fintech.homework.main

import ru.fintech.homework.base.MvpView

interface MainMvpView : MvpView {

    fun showDocumentFragment()

    fun closeDocumentFragment()

    fun hideDescription()

    fun showAddAnimation()

    fun showDeleteAnimation()

    fun showEmptyAnimation()
}
