package ru.fintech.homework.main

import android.os.Bundle

import ru.fintech.homework.R
import ru.fintech.homework.base.BaseActivity

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            replaceFragment(MainFragment.newInstance())
        }
    }
}
