package ru.fintech.homework.main

import ru.fintech.homework.base.BasePresenter

class MainPresenter<V : MainMvpView> : BasePresenter<V>(), MainMvpPresenter<V> {

    override fun addNewDocument() {
        getView()?.hideDescription()
        getView()?.showDocumentFragment()
        getView()?.showAddAnimation()
    }

    override fun deleteCurrentDocument(count: Int) {
        when (count) {
            1 -> {
                getView()?.closeDocumentFragment()
                getView()?.showEmptyAnimation()
            }
            0 ->  {
                getView()?.showEmptyAnimation()
            }
            else -> {
                getView()?.closeDocumentFragment()
                getView()?.showDeleteAnimation()
            }
        }
    }
}
