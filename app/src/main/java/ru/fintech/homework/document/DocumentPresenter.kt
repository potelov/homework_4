package ru.fintech.homework.document

import ru.fintech.homework.base.BasePresenter

class DocumentPresenter<V : DocumentMvpView> : BasePresenter<V>(), DocumentMvpPresenter<V> {

    override fun init() {
        getView()?.updateView()
    }
}