package ru.fintech.homework.document

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ru.fintech.homework.R
import ru.fintech.homework.base.BaseFragment

class DocumentFragment : BaseFragment(), DocumentMvpView {

    companion object {
        fun newInstance(): DocumentFragment {
            val args = Bundle()
            val fragment = DocumentFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var presenter: DocumentPresenter<DocumentMvpView>
    private lateinit var pageTitle: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        presenter = DocumentPresenter()
        presenter.onAttach(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_document, container, false)
        pageTitle = root.findViewById(R.id.tv_document_count)
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter.init()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
    }

    override fun updateView() {
        val count = fragmentManager?.backStackEntryCount
        pageTitle.text = "Документ №$count"
    }
}

