package ru.fintech.homework.document

import ru.fintech.homework.base.MvpPresenter

interface DocumentMvpPresenter<V : DocumentMvpView> : MvpPresenter<V> {

    fun init()
}