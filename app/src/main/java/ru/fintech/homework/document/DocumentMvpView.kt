package ru.fintech.homework.document

import ru.fintech.homework.base.MvpView

interface DocumentMvpView : MvpView {

    fun updateView()
}