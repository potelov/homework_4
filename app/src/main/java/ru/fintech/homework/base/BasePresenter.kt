package ru.fintech.homework.base

open class BasePresenter<V : MvpView> : MvpPresenter<V> {

    var mvpView: V? = null
        private set

    fun getView(): V? = mvpView

    val isViewAttached: Boolean
        get() = mvpView != null

    override fun onAttach(mvpView: V) {
        this.mvpView = mvpView
    }

    override fun onDetach() {
        mvpView = null
    }
}