package ru.fintech.homework.base

import androidx.fragment.app.Fragment

interface MvpView {

    fun replaceFragment(fragment: Fragment)

}
