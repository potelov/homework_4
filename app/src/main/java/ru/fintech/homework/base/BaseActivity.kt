package ru.fintech.homework.base

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import ru.fintech.homework.R

abstract class BaseActivity : AppCompatActivity(), MvpView {

    override fun replaceFragment(fragment: Fragment) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.addToBackStack(fragment.javaClass.name)
        if (supportFragmentManager.backStackEntryCount >= 1) {
            fragmentTransaction.setCustomAnimations(R.anim.tr_child_up, R.anim.tr_exit_left,
                    R.anim.tr_parent_back, R.anim.tr_child_back)
        }
        fragmentTransaction.replace(R.id.fragment_container, fragment, fragment.javaClass.name)
                .commitAllowingStateLoss()
    }

    override fun onBackPressed() {
        if (!handleBackPressed(supportFragmentManager)) {
            if (supportFragmentManager.backStackEntryCount > 1) {
                super.onBackPressed()
            } else {
                finish()
            }
        }
    }

    private fun handleBackPressed(manager: FragmentManager): Boolean {
        for (frag in manager.fragments) {
            if (frag == null) continue
            if (frag.isVisible && frag is BaseFragment) {
                if (frag.onBackPressed()) {
                    return true
                }
            }
        }
        return false
    }
}
